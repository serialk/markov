#!/usr/bin/env python3.2
#coding: utf-8
import sys
import pickle
import random

def random_sentence(chain):
    sentence = ['', '']
    sentence.append(random.choice(list(chain[''])))
    i = 2
    while(sentence[i][-1] != '.'):
            ref = ' '.join([sentence[i-1], sentence[i]]).strip()
            sentence.append(random.choice(list(chain[ref])))
            i += 1
    return ' '.join(sentence).strip()

if __name__ == '__main__':
    print('Importing the chain…', end=' ')
    f = open('markov.cache', 'rb')
    chain = pickle.load(f)
    print('done.')
    s = ''
    while True:
        while len(s) < 50 or len(s.split()) < 15:
            s = random_sentence(chain)
        print(s)#, end='')
        s = ''
        # input()
