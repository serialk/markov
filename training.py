#!/usr/bin/env python3.2
#coding: utf-8
import sys
import pickle

print('Splitting sentences…')
sentences = [[''] + (sentence + '.').split() for sentence in sys.stdin.read().split('.')]
chain = {'': set()}

print('Generating chain…')
last = -10
for cur, sentence in enumerate(sentences):
    if sentence != [''] and sentence[1] != '.':
        chain[''].add(sentence[1])
        i = 2
        while (i < len(sentence)):
            ref = ' '.join([sentence[i-2], sentence[i-1]]).strip()
            if ref not in chain:
                chain[ref] = set([sentence[i].strip()])
            else:
                chain[ref].add(sentence[i].strip())
            i+=1
    now = (cur * 100) // len(sentences)
    if now >= (last + 10):
        print('{0}% done.'.format(now))
        last = now
print('Saving chain…')
f = open('markov.cache', 'wb')
pickle.dump(chain, f)
