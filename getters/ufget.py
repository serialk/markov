#!/usr/bin/env python3
#coding: utf-8
from bs4 import BeautifulSoup
from bs4 import BeautifulStoneSoup
import urllib.request
import re

def get_list_thread():
    threads = set()
    for i in range(4):
        p = str(i + 1) if i > 1 else ''
        f = urllib.request.urlopen('http://www.underfoule.net/3615/list{}.html'.format(p))
        soup = BeautifulSoup(f.read())
        links = soup.find_all('a')
        for link in links:
            m = re.match(r'res/([0-9]+).html', str(link.get('href')))
            if m:
                threads.add(int(m.group(1)))
    return threads


def get_thread_content(tid):
    try:
        f = urllib.request.urlopen('http://www.underfoule.net/3615/res/{}.html'.format(tid))
    except:
        return ''
    soup = BeautifulSoup(f.read())
    content = soup.find_all('blockquote')
    comments = [quote.get_text() for quote in content]
    comments = [re.sub(r'>>[0-9]+(\s)*', '', s) for s in comments]
    return ''.join(comments)

def print_all():
    for tid in get_list_thread():
        print(get_thread_content(tid))

if __name__ == '__main__':
    print_all()
