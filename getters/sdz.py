#!/usr/bin/env python3
#coding: utf-8

from bs4 import BeautifulSoup
from bs4 import BeautifulStoneSoup
import urllib.request
import re


def get_list_threads(fid):
    threads = {}
    f = urllib.request.urlopen('http://www.siteduzero.com/forum-81-{}.html'.format(fid))
    soup = BeautifulSoup(f.read())
    soup_threads = soup.find_all('tr', ['sous_cat', 'sous_cat ligne_paire', 'sous_cat ligne_impaire'])
    for soup_thread in soup_threads:
        links = soup_thread.find_all('a')
        if links:
            m = re.match(r'forum-83-([0-9]+)-p1-(.+)\.html', str(links[0].get('href')))
            if m:
                tid = int(m.group(1))
                title = links[0].string
                if len(links) > 4:
                    re_pages = re.match(r'forum-83-([0-9]+)-p([0-9]+)-(.+)\.html', str(links[-4].get('href')))
                    pages = int(re_pages.group(2))
                else:
                    pages = 1
                threads[tid] = (title, pages)
    return threads


def get_list_forums():
    forums = {}
    f = urllib.request.urlopen('http://www.siteduzero.com/forum.html')
    soup = BeautifulSoup(f.read())
    links = soup.find_all('a')
    for link in links:
        m = re.match(r'forum-81-([0-9]+)-(.+)\.html', str(link.get('href')))
        if m:
            forumid = int(m.group(1))
            if forumid not in forums:
                forums[forumid] = link.string
    return forums


def get_thread_content(tid, page=1):
    f = urllib.request.urlopen('http://www.siteduzero.com/forum-83-{}-p{}.html'.format(tid, page))
    soup = BeautifulSoup(f.read())
    soup_table = soup.find('table', 'liste_messages').tbody
    soup_pseudos = soup_table.find_all('td', 'pseudo_membre')
    soup_msgs = soup_table.find_all('div', 'boite_message')
    for msg in soup_msgs:
        for elt in msg.find_all(['div', 'span', 'td', 'table'], ['signature', 'citation2', 'message_edite', 'citation', 'code', 'syntaxtable']):
            if hasattr(elt, 'contents'):
                elt.decompose()
    pseudos = [soup_pseudo.a.get_text() for soup_pseudo in soup_pseudos if soup_pseudo.a]
    messages = [soup_msg.get_text(strip=True) for soup_msg in soup_msgs if soup_msg]
    if(len(pseudos) < len(messages)):
        messages.pop(1)
    return list(zip(pseudos, messages))

def print_all():
    for fid in get_list_forums().keys():
        for tid, (threadtitle, pages) in get_list_threads(fid).items():
            for p in range(pages):
                for pseudo, msg in get_thread_content(tid, p+1):
                    if msg:
                        if msg[-1] != '.':
                            msg += '.'
                        print(msg)

if __name__ == '__main__':
    print_all()

